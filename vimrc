set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Plugin 'tpope/vim-fugitive.git'
Plugin 'scrooloose/syntastic.git'
Plugin 'klen/python-mode.git'
Plugin 'kien/ctrlp.vim.git'
Plugin 'hallison/vim-markdown.git'
Plugin 'altercation/vim-colors-solarized.git'
Plugin 'mrtazz/simplenote.vim.git'
Plugin 'mileszs/ack.vim'
Plugin 'justinmk/vim-sneak.git'
Plugin 'powerline/fonts'
Plugin 'bling/vim-airline'
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'davidhalter/jedi-vim'
Plugin 'tpope/vim-commentary'
Plugin 'Olical/vim-enmasse'
Plugin 'vimwiki/vimwiki'

filetype plugin indent on
syntax on


let g:ackprg="ag --nocolor --nogroup --column"

" Reload vimrc on save
autocmd! bufwritepost .vimrc source %

let g:rainbow_active = 1

" No extraneous writes
set nobackup
set nowritebackup
set noswapfile
set colorcolumn=80

" General syntastic config
let g:syntastic_aggregate_errors = 1

" jsx and javascript settings
let g:jsx_ext_required = 0
let g:javascript_enable_domhtmlcss = 1
let g:syntastic_javascript_checkers = ['eslint']

let g:syntastic_python_checkers = ["pyflakes"]
if !exists('g:syntastic_html_tidy_ignore_errors')
    let g:syntastic_html_tidy_ignore_errors = []
endif
let g:syntastic_html_tidy_ignore_errors = g:syntastic_html_tidy_ignore_errors + [
\ ' proprietary attribute "ng-',
\ ' proprietary attribute "ui-',
\ '<ng-include> is not recognized!',
\ 'discarding unexpected <ng-include>',
\ 'discarding unexpected </ng-include>',
\ '<ion-nav-bar> is not recognized!',
\ 'discarding unexpected <ion-nav-bar>',
\ 'discarding unexpected </ion-nav-bar>',
\ '<div> proprietary attribute "src'
\ ]

let g:vimwiki_list = [
\ {'path': '~/vimwiki/wikis/private_notes/', 'syntax': 'markdown', 'ext': '.md'},
\ {'path': '~/vimwiki/wikis/dancenotes', 'path_html': '~/vimwiki/html', 'syntax': 'markdown', 'ext': '.md'}
\ ]
let g:pymode_breakpoint = 0
let g:pymode_lint_checker = "pyflakes,pep8"
let g:pymode_lint_ignore = "E266,E402,E501,E303,W391,C901"
let g:pymode_rope = 0
let g:pymode_rope_completion = 0
let g:pymode_options = 1
let g:pymode_folding = 0
let g:pymode_motion = 1
let g:pymode_doc = 0
let g:pymode_virtualenv = 1

" Don't show a doc preview when doing autocompletion
set completeopt-=preview

if filereadable("~/.simplenoterc")
    let g:SimpltenoteFiletype="markdown"
    source ~/.simplenoterc
endif

filetype plugin indent on
" set 'selection', 'selectmode', 'mousemodel' and 'keymodel' for MS-Windows

behave mswin

" Prevents use of the cursor keys from stopping Select or Visual mode. Makes
" for more intuitive behavour under Windows.
set keymodel=startsel,stopsel
set noerrorbells
set vb t_vb=
autocmd GUIEnter * set vb t_vb=
set fencs=ucs-bom,utf-8,cp1250
set encoding=utf-8

" allow cursor to be positioned one char past end of line
" and apply operations to all of selection including last char
set selection=exclusive

if has("win32") || has("win64")
    set directory=$TMP
else
    set directory=/tmp
endif

if has("gui_running")
    let g:airline_powerline_fonts = 1
    set guifont=Inconsolata\ for\ Powerline\ Medium\ 12
else
    let g:airline_powerline_fonts = 0
    if !exists('g:airline_symbols')
        let g:airline_symbols = {}
    endif
    let g:airline_left_sep = ''
    let g:airline_left_alt_sep = ''
    let g:airline_right_sep = ''
    let g:airline_right_alt_sep = ''
    let g:airline_symbols.branch = ''
    let g:airline_symbols.readonly = ''
    let g:airline_symbols.linenr = ''
endif

" Turn off the toolbar and menubar
set guioptions-=T


syntax on
set sts=4
set expandtab
set tabstop=4
set shiftwidth=4
set textwidth=0
" allow backspacing over everything in insert mode
set backspace=indent,eol,start
set softtabstop=4
set autoindent
set smarttab
set smartindent
inoremap # X<BS>#

" Make searches case-sensitive only if they contain upper-case characters
set ignorecase
set smartcase

" show search matches as the search pattern is typed
set incsearch

" search-next wraps back to start of file
set wrapscan

set ruler
set relativenumber
set number
set hidden
set ic scs

set complete=.,w,b,u,t
set tw=0

" allow cursor keys to go right off end of one line, onto start of next
set whichwrap+=<,>,[,]

" no line wrapping
set nowrap

" when joining lines, don't insert two spaces after punctuation
set nojoinspaces

"make sure highlighting works all the way down long files
autocmd BufEnter * :syntax sync fromstart

" Keep more context when scrolling off the end of a buffer
set scrolloff=3

" Always show a tab line
set showtabline=2

" make tab completion for files/buffers act like bash
set wildmenu
set wildmode=longest,list

" show matching brackets, etc, for 1/10th of a second
set showmatch
set matchtime=1

" enable automatic yanking to and pasting from the selection
set clipboard+=unnamed

" map F3 to search jump thru grep results from copen
map <F3> :cnext<CR>

" Clean up trailing whitespace
nnoremap ,ctw mw:%s/ \+$//<CR>'w
autocmd FileType java,python,javascript autocmd BufWritePre <buffer> :%s/\s\+$//e

" Python support
let g:python_highlight_all = 1
autocmd FileType python setlocal cinwords=if,elif,else,for,while,try,except,finally,def,class

" Javascript
autocmd FileType javascript setlocal sts=4 expandtab shiftwidth=4 tabstop=4
au BufNewFile,BufRead *.js	setf javascript

" HTML files
autocmd FileType html setlocal sts=2 expandtab shiftwidth=2
au BufNewFile,BufRead *.html setf html.javascript

" Setup CTRL-N and CTRL-P for :cn and :cp (quickfix mode)
nmap <C-N> :cn<CR>
nmap <C-P> :cp<CR>

" Clean up trailing whitespace
nnoremap ,ctw mw:%s/ \+$//<CR>'w

" Prevent <C-n> from also acting when <C-space> used for completion
inoremap <Nul> <C-n>

" Make windows-ish bindings work (to keep everyone else happy)
" CTRL-C is Copy
vnoremap <C-C> "+y<ESC>

" CTRL-V is Paste
map <C-V> "+gP
nmap \\paste\\ "=@*.'xy'<CR>gPFx"_2x:echo<CR>
imap <C-V> x<Esc>\\paste\\"_xa
vmap <C-V> "-cx<Esc>\\paste\\"_x
cmap <C-V> <C-R>+

" CTRL-S for Save (in normal and insert mode)
nmap <C-s> :w<CR>
imap <C-s> <ESC>:w<CR>a

" Ctrl-<movement> to move between windows
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" ctrlp config
let g:ctrlp_extensions = ['tag', 'buffertag', 'quickfix', 'dir', 'rtscript',
            \ 'undo', 'line', 'changes']

let g:ctrlp_working_path_mode = 2

let g:ctrlp_custom_ignore = {
  \ 'dir':  '\.git$\|\.hg$\|\.svn$',
  \ 'file': '\.exe$\|\.so$\|\.dll$\|\.pyc$',
  \ 'link': 'some_bad_symbolic_links',
  \ }

if has("win32") || has("win64")
    set runtimepath+="C:\Program Files\git\bin"
endif
let g:ctrlp_user_command = {
  \ 'types': {
  \ 1: ['.git/', 'find %s -type d -name ".git" -prune -o -type f -a -not -name "*.pyc" -a -not -name "*.zip" -a -not -name "*.png" -a -not -name "*.jpg"'],
  \ 2: ['.hg/', 'find %s -type d -name ".hg" -prune -o -type f -a -not -name "*.pyc" -a -not -name "*.zip" -a -not -name "*.png" -a -not -name "*.jpg"'],
  \ },
  \ 'fallback': 'find %s -type f'
  \ }

let mapleader=","

" jedi completion
let g:jedi#goto_command = "<leader>g"
let g:jedi#goto_assignments_command = ""
let g:jedi#goto_definitions_command = ""
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>n"
let g:jedi#completions_command = "<C-Space>"
let g:jedi#rename_command = "<leader>r"
let g:jedi#smart_auto_mappings = 0

map <F10> :!ctags -R .<cr>
map <Leader>f :CtrlP<CR>
map <Leader>a :Ack <cword><CR>
map <Leader>h :e /cygdrive/c/Windows/System32/Drivers/etc/hosts<CR>
map <Leader>b :CtrlPBuffer<CR>
map <Leader>t :CtrlPTag<CR>
map <Leader>d :%s/def test/def DONTtest/<cr><c-o>
map <Leader>D :%s/def DONTtest/def test/<cr><c-o>
map <Leader><c-d> :%s/def test/def DONTtest/<cr><c-o>?def DONTtest<cr>:.,.s/def DONTtest/def test/<cr><c-o>

set noruler
set laststatus=2

if !has("gui_running")
    if &term == "xterm" || &term == "xterm-256color"
        set term=xterm-256color
    else
        set term=linux
    endif
endif

set background=light
let g:solarized_termcolors=256
colorscheme solarized

